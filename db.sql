DROP DATABASE IF EXISTS m07_uf4_pt1_faq;
CREATE DATABASE IF NOT EXISTS m07_uf4_pt1_faq;
USE m07_uf4_pt1_faq;

CREATE TABLE preguntes(
	id INT AUTO_INCREMENT PRIMARY KEY,
	text_pregunta VARCHAR(255)
)Engine=InnoDB;

CREATE TABLE respostes(
	id INT AUTO_INCREMENT PRIMARY KEY,
	id_pregunta INT NOT NULL,
	text_resposta VARCHAR(255),
	correcta INT(1),
	FOREIGN KEY (id_pregunta) REFERENCES preguntes (id)
)Engine=InnoDB;

INSERT INTO preguntes (text_pregunta) VALUES ("En quin institut estudies?");
INSERT INTO respostes (id_pregunta, text_resposta, correcta) VALUES (1, "IES Lluís Vives", 0);
INSERT INTO respostes (id_pregunta, text_resposta, correcta) VALUES (1, "IES Joan Boscà", 0);
INSERT INTO respostes (id_pregunta, text_resposta, correcta) VALUES (1, "IES Maristes", 0);
INSERT INTO respostes (id_pregunta, text_resposta, correcta) VALUES (1, "IES Ausiàs March", 1);

INSERT INTO preguntes (text_pregunta) VALUES ("Quina edat tens?");
INSERT INTO respostes (id_pregunta, text_resposta, correcta) VALUES (2, 10, 0);
INSERT INTO respostes (id_pregunta, text_resposta, correcta) VALUES (2, 15, 0);
INSERT INTO respostes (id_pregunta, text_resposta, correcta) VALUES (2, 20, 0);
INSERT INTO respostes (id_pregunta, text_resposta, correcta) VALUES (2, 25, 1);

INSERT INTO preguntes (text_pregunta) VALUES ("Tens animals?");
INSERT INTO respostes (id_pregunta, text_resposta, correcta) VALUES (3, "Sí", 1);
INSERT INTO respostes (id_pregunta, text_resposta, correcta) VALUES (3, "No", 0);
INSERT INTO respostes (id_pregunta, text_resposta, correcta) VALUES (3, "A vegades", 0);
INSERT INTO respostes (id_pregunta, text_resposta, correcta) VALUES (3, "Npi", 0);