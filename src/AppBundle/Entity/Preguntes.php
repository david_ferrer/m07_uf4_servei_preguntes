<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Preguntes
 *
 * @ORM\Table(name="preguntes")
 * @ORM\Entity
 */
class Preguntes
{
    /**
     * @var string
     *
     * @ORM\Column(name="text_pregunta", type="string", length=255, nullable=true)
     */
    private $textPregunta;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * Set textPregunta
     *
     * @param string $textPregunta
     *
     * @return Preguntes
     */
    public function setTextPregunta($textPregunta)
    {
        $this->textPregunta = $textPregunta;

        return $this;
    }

    /**
     * Get textPregunta
     *
     * @return string
     */
    public function getTextPregunta()
    {
        return $this->textPregunta;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    
}
