<?php

namespace AppBundle\Entity;

class RespostesRepository extends \Doctrine\ORM\EntityRepository
{
    public function findAllOrderedByName()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT r FROM AppBundle:Respostes r ORDER BY r.text_pregunta ASC'
            )
            ->getResult();
    }

}
