<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class PreguntesController extends Controller
{

    /**
     * @Route("/llistarPreguntes", name="llistarPreguntes")
     */
    public function selectAllQuestionsAction()
    {
        $questions = $this->getDoctrine()
            ->getRepository('AppBundle:Preguntes')
            ->findAll();

        if (count($questions)==0) {
            return $this->render('default/message.html.twig', array(
                'message' => 'No questions found'));
        }
        return $this->render('preguntes/content.html.twig', array(
            'questions' => $questions));
    }
    
    
}